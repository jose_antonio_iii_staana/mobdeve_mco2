package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models;

import java.io.Serializable;

public class Item implements Serializable {

    private long itemId;
    private String itemName;
    private String itemDescription;
    private StorageLocation storageLocation;
    private byte[] itemimage;

    public byte[] getItemimage() {
        return itemimage;
    }

    public void setItemimage(byte[] itemimage) {
        this.itemimage = itemimage;
    }

    //Item Constructors
    public Item() { }

    public Item(String itemName) {
        this.itemName = itemName;
    }

    //itemId Getters and Setters
    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    //itemNameGetters and Setters
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    //itemDescription Getters and Setters
    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) { this.itemDescription = itemDescription; }

    //storageLocation Getters and Setters
    public StorageLocation getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(StorageLocation storageLocation) { this.storageLocation = storageLocation; }

}
