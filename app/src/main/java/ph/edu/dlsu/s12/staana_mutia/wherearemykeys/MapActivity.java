package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;

public class MapActivity extends AppCompatActivity {

    private Button saveMapButton;
    private PaintView paintView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //Initialize views
        saveMapButton =  findViewById(R.id.button_save_map);
        paintView = findViewById(R.id.paintview_map);

        //Set paintView to cover the screen
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        //If the intent contains a Room Map, send this in the intent
        if (getIntent().hasExtra("roomMapByteArray")) {
            Bitmap roomMapBitMap = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("roomMapByteArray"),0,getIntent().getByteArrayExtra("roomMapByteArray").length);
            paintView.init(metrics, roomMapBitMap.copy(Bitmap.Config.ARGB_8888, true));
        } else {
            paintView.init(metrics);
        }

        saveMapButton.setOnClickListener(v -> {

            //Redirect
            Intent intent = new Intent();
            Bitmap bitmap = paintView.getBitmap();
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, bs);
            intent.putExtra("byteArray", bs.toByteArray());
            setResult(RESULT_OK, intent);
            finish();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pen:
                paintView.black();
                return true;
            case R.id.eraser:
                paintView.white();
                return true;
            case R.id.clear:
                paintView.clear();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
