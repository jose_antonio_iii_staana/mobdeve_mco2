package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.widget.ImageView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.DBHelper;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.RoomDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;

public class AddRoomActivity extends AppCompatActivity {

    private Function function;
    private EditText editTextName;
    private Button buttonAddRoom;
    private Bitmap roomImage = null;
    private Button buttonAddRoomImage;
    private ImageView imageViewRoom;
    private Bitmap roomMap = null;
    private Button buttonAddRoomMap;
    private ImageView imageViewRoomMap;

    private RoomDAO roomDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        //Initialize Views
        this.editTextName = findViewById(R.id.edittext_room_name);
        this.buttonAddRoom = findViewById(R.id.button_add_room);
        this.buttonAddRoomImage = findViewById(R.id.button_room_take_image);
        this.imageViewRoom = findViewById(R.id.imageview_room_image);
        this.buttonAddRoomMap = findViewById(R.id.button_room_draw_map);
        this.imageViewRoomMap =  findViewById(R.id.imageview_room_map);

        this.roomDAO = new RoomDAO(this);

        //Retrieve Room Image from Camera
        ActivityResultLauncher<Intent> takeImageResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> { if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    roomImage = (Bitmap) data.getExtras().get("data");
                    if (roomImage != null)
                    {
                        imageViewRoom.setImageBitmap(roomImage);
                    }
                }
                });

        buttonAddRoomImage.setOnClickListener(v ->{
            Intent takeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takeImageResultLauncher.launch(takeImageIntent);
        });

        //Retrieve Room Map from Map Activity
        ActivityResultLauncher<Intent> drawMapResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> { if (result.getResultCode() == Activity.RESULT_OK) {

                    //Re-encode byteArray into Bitmap
                    Bitmap roomBitMap = BitmapFactory.decodeByteArray(
                            result.getData().getByteArrayExtra("byteArray"),0, result.getData().getByteArrayExtra("byteArray").length);

                    roomMap = roomBitMap;
                    if (roomMap != null)
                    {
                        imageViewRoomMap.setImageBitmap(roomMap);
                    }
                }
                });

        buttonAddRoomMap.setOnClickListener(v ->{
            Intent drawMapIntent = new Intent(AddRoomActivity.this, MapActivity.class);
            drawMapResultLauncher.launch(drawMapIntent);
        });

        //Create new Room and add it to the database
        buttonAddRoom.setOnClickListener(v -> {

            Editable roomName = editTextName.getText();
            BitmapDrawable bitmapDrawableImage = (BitmapDrawable) imageViewRoom.getDrawable();
            BitmapDrawable bitmapDrawableMap = (BitmapDrawable) imageViewRoomMap.getDrawable();

            if(!TextUtils.isEmpty(roomName)
                    && bitmapDrawableImage != null
                    && bitmapDrawableMap != null) {

                //Convert Image Bitmap to Byte Array to store in Database
                roomImage = bitmapDrawableImage.getBitmap();
                byte[] roomImageByte = function.getBytes(roomImage);

                //Convert Map Bitmap to Byte Array to store in Database
                roomMap = bitmapDrawableMap.getBitmap();
                byte[] roomMapByte = function.getBytes(roomMap);

                //Add new Room to the Database
                Room newRoom = roomDAO.createRoom(
                        roomName.toString(),
                        roomImageByte,
                        roomMapByte
                );

                //Redirect back to list of Rooms
                Intent intent = new Intent();
                intent.putExtra(RoomActivity.EXTRA_ADDED_ROOM, newRoom);
                setResult(RESULT_OK, intent);
                Toast.makeText(v.getContext(), "Room created!", Toast.LENGTH_SHORT).show();
                finish();

            }
            else {
                Toast.makeText(v.getContext(), "Required fields are empty", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        roomDAO.close();
    }

}