package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.RoomDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.StorageLocationDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class AddStorageLocationActivity extends AppCompatActivity {

    public static final String EXTRA_ADDED_STORAGE_LOCATION = "extra_key_added_storage_location";
    private Function function;
    private EditText editTextStorageLocationName;
    private Button buttonAddStorageLocation;
    private Bitmap storageLocationImage = null;
    private Button buttonAddStorageLocationImage;
    private ImageView imageviewStorageLocation;
    private Bitmap storageLocationMap = null;
    private Button buttonAddStorageLocationMap;
    private ImageView imageViewStorageLocationMap;

    private RoomDAO roomDAO;
    private StorageLocationDAO storageLocationDAO;

    private Room selectedRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_storage_location);

        //Initialize Views
        this.editTextStorageLocationName = findViewById(R.id.edittext_storage_location_name);
        this.buttonAddStorageLocation = findViewById(R.id.button_add_storage_location);
        this.buttonAddStorageLocationImage = findViewById(R.id.button_storage_location_take_image);
        this.imageviewStorageLocation = findViewById(R.id.imageview_storage_location_image);
        this.buttonAddStorageLocationMap = findViewById(R.id.button_storage_location_draw_map);
        this.imageViewStorageLocationMap =  findViewById(R.id.imageview_storage_location_map);

        this.roomDAO = new RoomDAO(this);
        this.storageLocationDAO = new StorageLocationDAO(this);
        selectedRoom = roomDAO.getRoomById(getIntent().getLongExtra("ROOM_ID", 0));

        //Retrieve Storage Location Image from Camera
        ActivityResultLauncher<Intent> takeImageResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> { if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    storageLocationImage = (Bitmap) data.getExtras().get("data");
                    if (storageLocationImage != null)
                    {
                        imageviewStorageLocation.setImageBitmap(storageLocationImage);
                    }
                }
                });

        buttonAddStorageLocationImage.setOnClickListener(v ->{
            Intent takeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takeImageResultLauncher.launch(takeImageIntent);
        });

        //Retrieve Storage Location Map from Map Activity
        ActivityResultLauncher<Intent> drawMapResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> { if (result.getResultCode() == Activity.RESULT_OK) {

                    //Re-encode byteArray into Bitmap
                    Bitmap storageLocationBitMap = BitmapFactory.decodeByteArray(
                            result.getData().getByteArrayExtra("byteArray"),0, result.getData().getByteArrayExtra("byteArray").length);

                    storageLocationMap = storageLocationBitMap;
                    if (storageLocationMap != null)
                    {
                        imageViewStorageLocationMap.setImageBitmap(storageLocationMap);
                    }
                }
                });

        buttonAddStorageLocationMap.setOnClickListener(v ->{
            Intent drawMapIntent = new Intent(AddStorageLocationActivity.this, MapActivity.class);
            drawMapIntent.putExtra("roomMapByteArray", selectedRoom.getRoomMap());
            drawMapResultLauncher.launch(drawMapIntent);
        });

        //Create new Storage Location and add it to the database
        buttonAddStorageLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Editable storageLocationName = editTextStorageLocationName.getText();
                BitmapDrawable bitmapDrawableImage = (BitmapDrawable) imageviewStorageLocation.getDrawable();
                BitmapDrawable bitmapDrawableMap = (BitmapDrawable) imageViewStorageLocationMap.getDrawable();

                if(!TextUtils.isEmpty(storageLocationName)
                        && selectedRoom != null
                        && bitmapDrawableImage != null
                        && bitmapDrawableMap != null) {

                    //Convert Image Bitmap to Byte Array to store in Database
                    storageLocationImage = bitmapDrawableImage.getBitmap();
                    byte[] storageLocationImageByte = function.getBytes(storageLocationImage);

                    //Convert Map Bitmap to Byte Array to store in Database
                    storageLocationMap = bitmapDrawableMap.getBitmap();
                    byte[] storageLocationMapByte = function.getBytes(storageLocationMap);

                    //Add new Storage Location to the Database
                    StorageLocation newStorageLocation = storageLocationDAO.createStorageLocation(
                            storageLocationName.toString(),
                            selectedRoom.getRoomId(),
                            storageLocationImageByte,
                            storageLocationMapByte
                    );

                    //Redirect back to list of Rooms
                    Intent intent = new Intent();
                    intent.putExtra(StorageLocationActivity.EXTRA_ADDED_STORAGE_LOCATION, newStorageLocation);
                    setResult(RESULT_OK, intent);
                    Toast.makeText(v.getContext(), "Storage Location created!", Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    Toast.makeText(v.getContext(), "Required fields are empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        roomDAO.close();
        storageLocationDAO.close();
    }

}
