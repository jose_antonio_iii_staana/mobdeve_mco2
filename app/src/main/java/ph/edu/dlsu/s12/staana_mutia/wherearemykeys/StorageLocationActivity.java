package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters.ListRoomsAdapter;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters.ListStorageLocationsAdapter;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.RoomDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.StorageLocationDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class StorageLocationActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener  {

    public static final String EXTRA_ADDED_STORAGE_LOCATION = "extra_key_added_storage_location";
    public static final String EXTRA_SELECTED_ROOM_ID = "extra_key_selected_room_id";

    private ListView listViewStorageLocations;
    private TextView textViewEmptyListStorageLocations;
    private ImageView imageViewStorageLocationRoomMap;
    private ImageView imageView;
    private Button buttonAddStorageLocation;
    private Button buttonTakeImage;

    private ListStorageLocationsAdapter adapter;
    private List<StorageLocation> listStorageLocations;
    private StorageLocationDAO storageLocationDAO;
    private RoomDAO roomDAO;
    private Room selectedRoom;
    private Context context;

    private int imageId =-1;
    private long roomId = -1;
    private Bitmap storageImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage_location);

        //Initialize Views
        this.listViewStorageLocations = findViewById(R.id.listview_storage_locations);
        this.textViewEmptyListStorageLocations = findViewById(R.id.textview_empty_list_storage_locations);
        this.buttonAddStorageLocation = findViewById(R.id.button_add_storage_location);
        this.imageViewStorageLocationRoomMap = findViewById(R.id.imageview_storage_location_room_map);
        this.imageView = findViewById(R.id.imageview_storage_location_image);
        this.buttonTakeImage = findViewById(R.id.button_storage_location_take_image);
        this.listViewStorageLocations.setOnItemClickListener(this);
        this.listViewStorageLocations.setOnItemLongClickListener(this);

        this.roomDAO = new RoomDAO(this);
        this.storageLocationDAO = new StorageLocationDAO(this);

        Intent intent = getIntent();
        if (intent != null) {
            this.roomId = intent.getLongExtra(EXTRA_SELECTED_ROOM_ID, -1);
        }

        if(roomId != -1) {
            //Set image
            selectedRoom = roomDAO.getRoomById(roomId);
            imageViewStorageLocationRoomMap.setImageBitmap(Function.getImage(selectedRoom.getRoomMap()));

            listStorageLocations = storageLocationDAO.getStorageLocationsInRoom(roomId);

            //Fill ListView
            context = this;
            if (listStorageLocations != null && !listStorageLocations.isEmpty()) {
                adapter = new ListStorageLocationsAdapter(this, listStorageLocations);
                listViewStorageLocations.setAdapter(adapter);
            } else {
                //Placeholder if there are no Storage Locations
                textViewEmptyListStorageLocations.setVisibility(View.VISIBLE);
                listViewStorageLocations.setVisibility(View.GONE);
            }

            ActivityResultLauncher<Intent> addStorageLocationActivityResultLauncher = registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            if(data != null) {
                                StorageLocation newStorageLocation = (StorageLocation) data.getSerializableExtra(EXTRA_ADDED_STORAGE_LOCATION);

                                //Add Storage Location to List
                                if(newStorageLocation != null) {
                                    if(listStorageLocations == null) {
                                        listStorageLocations = new ArrayList<>();
                                    }
                                    listStorageLocations.add(newStorageLocation);

                                    //Toggle placeholder
                                    if(listViewStorageLocations.getVisibility() != View.VISIBLE) {
                                        listViewStorageLocations.setVisibility(View.VISIBLE);
                                        textViewEmptyListStorageLocations.setVisibility(View.GONE);
                                    }

                                    if(adapter == null) {
                                        adapter = new ListStorageLocationsAdapter(context, listStorageLocations);
                                        listViewStorageLocations.setAdapter(adapter);
                                    }

                                    //Refresh ListView
                                    adapter.setItems(listStorageLocations);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    });

            buttonAddStorageLocation.setOnClickListener(v -> {
                Intent addRoomIntent = new Intent(StorageLocationActivity.this, AddStorageLocationActivity.class);
                addRoomIntent.putExtra("ROOM_ID", roomId);
                addStorageLocationActivityResultLauncher.launch(addRoomIntent);
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        storageLocationDAO.close();
        roomDAO.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StorageLocation clickedStorageLocation = adapter.getItem(position);
        Intent intent = new Intent (this, ItemActivity.class);
        intent.putExtra(ItemActivity.EXTRA_SELECTED_STORAGE_LOCATION_ID, clickedStorageLocation.getStorageLocationId());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        StorageLocation clickedStorageLocation = adapter.getItem(position);
        showDeleteDialog(clickedStorageLocation);
        return true;
    }

    private void showDeleteDialog(final StorageLocation clickedStorageLocation) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Delete");
        alertDialogBuilder.setMessage("Delete \"" + clickedStorageLocation.getStorageLocationName() + "\" from your list of Storage Locations?");

        alertDialogBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            //Delete clickedStorageLocation and Refresh ListView
            if(storageLocationDAO != null) {
                storageLocationDAO.deleteStorageLocation(clickedStorageLocation);
                listStorageLocations.remove(clickedStorageLocation);

                //Refresh ListView
                if(listStorageLocations.isEmpty()) {
                    listStorageLocations = null;
                    listViewStorageLocations.setVisibility(View.GONE);
                    textViewEmptyListStorageLocations.setVisibility(View.VISIBLE);
                }
                adapter.setItems(listStorageLocations);
                adapter.notifyDataSetChanged();
            }

            dialog.dismiss();
            Toast.makeText(StorageLocationActivity.this, "Storage Location deleted", Toast.LENGTH_SHORT).show();
        });

        alertDialogBuilder.setNeutralButton(android.R.string.no, (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
