package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private TextView textViewName;
    private EditText editTextPassword;
    private Button buttonLogin;

    private String name;
    private String password;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Redirect to the registration menu if the app has no user information registered
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
            finish();
        }

        //Initialize views
        textViewName = findViewById(R.id.textview_login_name);
        editTextPassword = findViewById(R.id.edittext_password);
        buttonLogin = findViewById(R.id.button_login);

        //Retrieve user information from sharedPreferences
        sharedPreferences = getApplicationContext().getSharedPreferences("MyUserPrefs", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", "");
        password = sharedPreferences.getString("password", "");

        textViewName.setText(name);

        buttonLogin.setOnClickListener(v -> {
            if (password.equals(editTextPassword.getText().toString())) {
                //Redirect to Room list if passwords match
                Intent intent = new Intent(LoginActivity.this, RoomActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(v.getContext(), "PIN does not match records. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
