package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models;

import java.io.Serializable;

public class Room implements Serializable {

    private long roomId;
    private String roomName;
    private byte[] roomImage;
    private byte[] roomMap;

    //Room Constructors
    public Room() { }

    public Room (String roomName) {
        this.roomName = roomName;
    }

    //roomId Getter & Setter
    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    //roomName Getter & Setter
    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    //roomImage Getter & Setter
    public byte[] getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(byte[] roomImage) {
        this.roomImage = roomImage;
    }

    //roomMap Getter & Setter
    public byte[] getRoomMap() { return roomMap; }

    public void setRoomMap(byte[] roomMap) { this.roomMap = roomMap; }

}
