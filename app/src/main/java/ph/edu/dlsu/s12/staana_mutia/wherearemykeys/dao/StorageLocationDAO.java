package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Item;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class StorageLocationDAO {

    private Context context;

    //Database fields
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allColumns = {
            DBHelper.COLUMN_STORAGE_LOCATION_ID,
            DBHelper.COLUMN_STORAGE_LOCATION_NAME,
            DBHelper.COLUMN_STORAGE_LOCATION_ROOM_ID,
            DBHelper.COLUMN_STORAGE_LOCATION_IMAGE,
            DBHelper.COLUMN_STORAGE_LOCATION_MAP
    };

    public StorageLocationDAO (Context context) {
        dbHelper = new DBHelper(context);
        this.context = context;

        try {
            open();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public StorageLocation createStorageLocation(String storageLocationName, long roomId, byte[] image,  byte[] map) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_STORAGE_LOCATION_NAME, storageLocationName);
        values.put(DBHelper.COLUMN_STORAGE_LOCATION_ROOM_ID, roomId);
        values.put(DBHelper.COLUMN_STORAGE_LOCATION_IMAGE, image);
        values.put(DBHelper.COLUMN_STORAGE_LOCATION_MAP, map);
        long insertID = database.insert(DBHelper.TABLE_STORAGE_LOCATIONS, null, values);
        Cursor cursor = database.query(DBHelper.TABLE_STORAGE_LOCATIONS,
                allColumns, DBHelper.COLUMN_STORAGE_LOCATION_ID + " = " + insertID,
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        StorageLocation newStorageLocation = cursorToStorageLocation(cursor);
        cursor.close();
        return newStorageLocation;
    }

    public StorageLocation getStorageLocationById(long id) {
        Cursor cursor = database.query(DBHelper.TABLE_STORAGE_LOCATIONS, allColumns,
                DBHelper.COLUMN_STORAGE_LOCATION_ID + " = ?",
                new String[] {String.valueOf(id)},
                null,
                null,
                null);

        if ( cursor != null) {
            cursor.moveToFirst();
        }

        StorageLocation storageLocation = cursorToStorageLocation(cursor);
        return storageLocation;
    }

    public void deleteStorageLocation(StorageLocation storageLocation) {
        long id = storageLocation.getStorageLocationId();

        //Delete all Items in this Storage Location
        ItemDAO itemDAO = new ItemDAO(context);
        List<Item> listItems = itemDAO.getItemsInStorageLocation(id);
        if (listItems != null && !listItems.isEmpty()) {
            for (Item item : listItems) {
                itemDAO.deleteItem(item);
            }
        }

        System.out.println("StorageLocation " + id + " was deleted");
        database.delete(DBHelper.TABLE_STORAGE_LOCATIONS,
                DBHelper.COLUMN_STORAGE_LOCATION_ID + " = " + id,
                null);
    }

    public List<StorageLocation> getAllStorageLocations() {
        List<StorageLocation> listStorageLocations = new ArrayList<StorageLocation>();

        Cursor cursor = database.query(DBHelper.TABLE_STORAGE_LOCATIONS, allColumns,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            StorageLocation storageLocation = cursorToStorageLocation(cursor);
            listStorageLocations.add(storageLocation);
            cursor.moveToNext();
        }

        cursor.close();
        return listStorageLocations;
    }

    public List<StorageLocation> getStorageLocationsInRoom (long roomId) {
        List<StorageLocation> listStorageLocations = new ArrayList<StorageLocation>();

        Cursor cursor = database.query(DBHelper.TABLE_STORAGE_LOCATIONS, allColumns,
                DBHelper.COLUMN_STORAGE_LOCATION_ROOM_ID + " = ?",
                new String[]{String.valueOf(roomId)},
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            StorageLocation storageLocation = cursorToStorageLocation(cursor);
            listStorageLocations.add(storageLocation);
            cursor.moveToNext();
        }

        cursor.close();
        return listStorageLocations;
    }

    private StorageLocation cursorToStorageLocation(Cursor cursor) {
        StorageLocation storageLocation = new StorageLocation();
        storageLocation.setStorageLocationId(cursor.getLong(0));
        storageLocation.setStorageLocationName(cursor.getString(1));
        storageLocation.setStorageLocationImage(cursor.getBlob(3));
        storageLocation.setStorageLocationImage(cursor.getBlob(4));

        long roomId = cursor.getLong(2);
        RoomDAO dao = new RoomDAO(context);
        Room room = dao.getRoomById(roomId);
        if(room != null)
            storageLocation.setRoom(room);

        return storageLocation;
    }
}
