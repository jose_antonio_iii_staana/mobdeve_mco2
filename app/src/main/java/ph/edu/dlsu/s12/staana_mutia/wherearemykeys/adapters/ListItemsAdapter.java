package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.Function;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.R;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Item;

public class ListItemsAdapter extends BaseAdapter {

    private Function function;
    private List<Item> items;
    private LayoutInflater inflater;

    public ListItemsAdapter(Context context, List<Item> listItems) {
        this.setItems(listItems);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().size() : 0;
    }

    @Override
    public Item getItem(int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position) : null ;
    }

    @Override
    public long getItemId(int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position).getItemId() : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            v = inflater.inflate(R.layout.list_item_item, parent, false);
            holder = new ViewHolder();
            holder.textViewItemName = v.findViewById((R.id.textview_item_name));
            holder.textViewItemDescription = v.findViewById(R.id.textview_item_description);
            holder.imageViewItem = v.findViewById(R.id.imageView_item_image);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        // Fill Row Data
        Item currentItem = getItem(position);
        if(currentItem != null) {
            holder.textViewItemName.setText(currentItem.getItemName());
            holder.textViewItemDescription.setText(currentItem.getItemDescription());
            holder.imageViewItem.setImageBitmap(function.getImage(currentItem.getItemimage()));
        }

        return v;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    class ViewHolder {
        ImageView imageViewItem;
        TextView textViewItemName;
        TextView textViewItemDescription;
    }
}
