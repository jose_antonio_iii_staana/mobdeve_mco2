package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button buttonRegister;

    private String name;
    private String password;
    private String confirmPassword;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Initialize views
        editTextName = findViewById(R.id.edittext_name);
        editTextPassword = findViewById(R.id.edittext_password);
        editTextConfirmPassword = findViewById(R.id.edittext_confirm_password);
        buttonRegister = findViewById(R.id.button_register);

        //Assert that if the registration menu has been accessed, the user has not been registered yet
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstRun", true).apply();

        sharedPreferences = getSharedPreferences("MyUserPrefs", Context.MODE_PRIVATE);

        buttonRegister.setOnClickListener(v -> {
            //Retrieve user information from user input
            name = editTextName.getText().toString();
            password = editTextPassword.getText().toString();
            confirmPassword = editTextConfirmPassword.getText().toString();

            if (password.equals(confirmPassword)) {
                if(password.length() > 3 && confirmPassword.length() > 3) {
                    //Register user information into sharedPreferences if passwords are over 3 characters and match
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("name", name);
                    editor.putString("password", password);
                    editor.apply();
                    Toast.makeText(v.getContext(), "Information Saved.", Toast.LENGTH_SHORT).show();

                    //Set isFirstRun flag to false so that the register activity will not be called again
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply();

                    //Redirect to Room list after successful registration
                    Intent intent = new Intent(RegisterActivity.this, RoomActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(v.getContext(), "Minimum PIN length is 4. Please try again.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(v.getContext(), "PINs do not match. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
