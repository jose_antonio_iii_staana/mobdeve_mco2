package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.widget.*;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters.ListRoomsAdapter;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.RoomDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;

public class RoomActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    public static final String EXTRA_ADDED_ROOM = "extra_key_added_room";

    private ListView listViewRooms;
    private TextView textViewEmptyListRooms;
    private Button buttonAddRoom;

    private ListRoomsAdapter adapter;
    private List<Room> listRooms;
    private RoomDAO roomDAO;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        //Initialize Views
        this.listViewRooms = findViewById(R.id.listview_rooms);
        this.textViewEmptyListRooms = findViewById(R.id.textview_empty_list_rooms);
        this.buttonAddRoom = findViewById(R.id.button_add_room);
        this.listViewRooms.setOnItemClickListener(this);
        this.listViewRooms.setOnItemLongClickListener(this);

        //Fill ListView
        context = this;
        roomDAO = new RoomDAO(this);
        listRooms = roomDAO.getAllRooms();
        if (listRooms != null && !listRooms.isEmpty()) {
            adapter = new ListRoomsAdapter(this, listRooms);
            listViewRooms.setAdapter(adapter);
        } else {
            //Placeholder if there are no Rooms
            textViewEmptyListRooms.setVisibility(View.VISIBLE);
            listViewRooms.setVisibility(View.GONE);
        }

        ActivityResultLauncher<Intent> addRoomActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        if(data != null) {
                            Room newRoom = (Room) data.getSerializableExtra(EXTRA_ADDED_ROOM);

                            //Add Room to List
                            if(newRoom != null) {
                                if(listRooms == null) {
                                    listRooms = new ArrayList<>();
                                }
                                listRooms.add(newRoom);

                                //Toggle placeholder
                                if(listViewRooms.getVisibility() != View.VISIBLE) {
                                    listViewRooms.setVisibility(View.VISIBLE);
                                    textViewEmptyListRooms.setVisibility(View.GONE);
                                }

                                if(adapter == null) {
                                    adapter = new ListRoomsAdapter(context, listRooms);
                                    listViewRooms.setAdapter(adapter);
                                }

                                //Refresh ListView
                                adapter.setItems(listRooms);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });

        buttonAddRoom.setOnClickListener(v -> {
            Intent intent = new Intent(RoomActivity.this, AddRoomActivity.class);
            addRoomActivityResultLauncher.launch(intent);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        roomDAO.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Room clickedRoom = adapter.getItem(position);
        Intent intent = new Intent (this, StorageLocationActivity.class);
        intent.putExtra(StorageLocationActivity.EXTRA_SELECTED_ROOM_ID, clickedRoom.getRoomId());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Room clickedRoom = adapter.getItem(position);
        showDeleteDialog(clickedRoom);
        return true;
    }

    private void showDeleteDialog(final Room clickedRoom) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Delete");
        alertDialogBuilder.setMessage("Delete \"" + clickedRoom.getRoomName() + "\" from your list of Rooms?");

        alertDialogBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            //Delete clickedRoom and Refresh ListView
            if(roomDAO != null) {
                roomDAO.deleteRoom(clickedRoom);
                listRooms.remove(clickedRoom);

                //Refresh ListView
                if(listRooms.isEmpty()) {
                    listRooms = null;
                    listViewRooms.setVisibility(View.GONE);
                    textViewEmptyListRooms.setVisibility(View.VISIBLE);
                }
                adapter.setItems(listRooms);
                adapter.notifyDataSetChanged();
            }

            dialog.dismiss();
            Toast.makeText(RoomActivity.this, "Room deleted", Toast.LENGTH_SHORT).show();
        });

        alertDialogBuilder.setNeutralButton(android.R.string.no, (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}