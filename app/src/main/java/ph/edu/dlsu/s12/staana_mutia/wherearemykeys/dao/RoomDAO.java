package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.Function;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class RoomDAO {
    private Function function;

    //Database fields
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private Context context;
    private String[] allColumns = {
            DBHelper.COLUMN_ROOM_ID,
            DBHelper.COLUMN_ROOM_NAME,
            DBHelper.COLUMN_ROOM_IMAGE,
            DBHelper.COLUMN_ROOM_MAP
    };

    public RoomDAO(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);

        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() throws SQLException {
        dbHelper.close();
    }

    public Room createRoom(String room_name, byte[] image, byte[] map) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_ROOM_NAME, room_name);
        values.put(DBHelper.COLUMN_ROOM_IMAGE, image);
        values.put(DBHelper.COLUMN_ROOM_MAP, map);
        long insertId = database.insert(DBHelper.TABLE_ROOMS,
                null, values);
        Cursor cursor = database.query(
                DBHelper.TABLE_ROOMS,
                allColumns,
                DBHelper.COLUMN_ROOM_ID + " = " + insertId,
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        Room newRoom = cursorToRoom(cursor);
        cursor.close();
        return  newRoom;
    }

    public void deleteRoom(Room room) {
        long id = room.getRoomId();

        //Delete all StorageLocations in this Room
        StorageLocationDAO storageLocationDAO = new StorageLocationDAO(context);
        List<StorageLocation> listStorageLocations = storageLocationDAO.getStorageLocationsInRoom(id);
        if (listStorageLocations != null && !listStorageLocations.isEmpty()) {
            for (StorageLocation storageLocation : listStorageLocations) {
                storageLocationDAO.deleteStorageLocation(storageLocation);
            }
        }

        System.out.println("Room " + id + " was deleted");
        database.delete(DBHelper.TABLE_ROOMS,
                DBHelper.COLUMN_ROOM_ID + " = " + id,
                null);
    }

    public List<Room> getAllRooms() {
        List<Room> listRooms = new ArrayList<Room>();

        Cursor cursor = database.query(DBHelper.TABLE_ROOMS, allColumns,
                null,
                null,
                null,
                null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            while ((!cursor.isAfterLast())) {
                Room room = cursorToRoom(cursor);
                listRooms.add(room);
                cursor.moveToNext();
            }

            cursor.close();
        }
        return listRooms;
    }

    public Room getRoomById(long id) {
        Cursor cursor = database.query(DBHelper.TABLE_ROOMS, allColumns,
                DBHelper.COLUMN_ROOM_ID + " = ?",
                new String[] {String.valueOf(id)},
                null,
                null,
                null);

        if ( cursor != null) {
            cursor.moveToFirst();
        }

        Room room = cursorToRoom(cursor);
        return room;
    }

    protected Room cursorToRoom(Cursor cursor) {
        Room room = new Room();
        room.setRoomId(cursor.getLong(0));
        room.setRoomName(cursor.getString(1));
        room.setRoomImage(cursor.getBlob(2));
        room.setRoomMap(cursor.getBlob(3));
        return room;
    }

}
