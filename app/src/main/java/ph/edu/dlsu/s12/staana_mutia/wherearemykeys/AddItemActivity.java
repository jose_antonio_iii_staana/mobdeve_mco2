package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.ItemDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.RoomDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.StorageLocationDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Item;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class AddItemActivity extends AppCompatActivity {

    private Function function;
    private EditText editTextItemName;
    private EditText editTextItemDescription;
    private Button buttonAddItem;
    private Button buttonaddItemImage;
    private ImageView imageViewItem;

    private Bitmap itemImage;

    private StorageLocationDAO storageLocationDAO;
    private ItemDAO itemDAO;

    private StorageLocation selectedStorageLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        //Initialize Views
        this.editTextItemName = findViewById(R.id.edittext_item_name);
        this.editTextItemDescription = findViewById(R.id.edittext_item_description);
        this.buttonAddItem = findViewById(R.id.button_add_item);
        this.buttonaddItemImage = findViewById(R.id.button_item_take_image);
        this.imageViewItem = findViewById(R.id.imageview_item_image);

        this.storageLocationDAO = new StorageLocationDAO(this);
        this.itemDAO = new ItemDAO(this);
        selectedStorageLocation = storageLocationDAO.getStorageLocationById(getIntent().getLongExtra("STORAGE_LOCATION_ID", 0));

        //Retrieve Item Image from Camera
        ActivityResultLauncher<Intent> takeImageResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> { if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    itemImage = (Bitmap) data.getExtras().get("data");
                    if (itemImage != null)
                    {
                        imageViewItem.setImageBitmap(itemImage);
                    }
                }
                });

        buttonaddItemImage.setOnClickListener(v ->{
            Intent takeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takeImageResultLauncher.launch(takeImageIntent);
        });

        //Create new Item and add it to the database
        buttonAddItem.setOnClickListener(v -> {
            Editable itemName = editTextItemName.getText();
            Editable itemDescription = editTextItemDescription.getText();
            BitmapDrawable drawable = (BitmapDrawable) imageViewItem.getDrawable();

            if(!TextUtils.isEmpty(itemName)
                    && !TextUtils.isEmpty(itemDescription)
                    && selectedStorageLocation != null
                    && drawable != null) {

                //Convert Image Bitmap to Byte Array to store in Databasea
                itemImage = drawable.getBitmap();
                byte[] itemImageByte = function.getBytes(itemImage);

                //Add new Item to the Database
                Item newItem = itemDAO.createItem(
                        itemName.toString(),
                        itemDescription.toString(),
                        selectedStorageLocation.getStorageLocationId(),
                        itemImageByte
                );

                //Redirect back to list of Items
                Intent intent = new Intent();
                intent.putExtra(ItemActivity.EXTRA_ADDED_ITEM, newItem);
                setResult(RESULT_OK, intent);
                Toast.makeText(v.getContext(), "Item created!", Toast.LENGTH_SHORT).show();
                finish();

            } else {
                Toast.makeText(v.getContext(), "Required fields are empty", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        storageLocationDAO.close();
        itemDAO.close();
    }

}
