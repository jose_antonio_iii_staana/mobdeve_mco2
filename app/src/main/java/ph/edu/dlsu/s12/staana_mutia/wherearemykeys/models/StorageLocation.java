package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.sql.Blob;

public class StorageLocation implements Serializable {

    private long storageLocationId;
    private String storageLocationName;
    private byte[] storageLocationImage;
    private byte[] storageLocationMap;
    private Room room;

    //StorageLocation Constructors
    public StorageLocation() { }

    public StorageLocation (String storageLocationName) { this.storageLocationName = storageLocationName; }

    //storageLocationId  Getter & Setter
    public long getStorageLocationId() { return storageLocationId; }

    public void setStorageLocationId(long storageLocationId) { this.storageLocationId = storageLocationId; }

    //storageLocationName Getter & Setter
    public String getStorageLocationName() { return storageLocationName; }

    public void setStorageLocationName(String storageLocationName) { this.storageLocationName = storageLocationName; }

    //room Getter & Setter
    public Room getRoom() { return room; }

    public void setRoom(Room room) {
        this.room = room;
    }

    //storageLocationImage Getter & Setter
    public byte[] getStorageLocationImage() {
        return storageLocationImage;
    }

    public void setStorageLocationImage(byte[] storageLocationImage) { this.storageLocationImage = storageLocationImage; }

    //storageLocationMap Getter & Setter
    public byte[] getStorageLocationMap() { return storageLocationMap; }

    public void setStorageLocationMap(byte[] storageLocationMap) { this.storageLocationMap = storageLocationMap; }
}
