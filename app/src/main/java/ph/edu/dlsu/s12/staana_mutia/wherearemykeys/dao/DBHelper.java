package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Blob;

public class DBHelper extends SQLiteOpenHelper {

    //Columns of Room table
    public static final String TABLE_ROOMS = "rooms";
    public static final String COLUMN_ROOM_ID = "_id";
    public static final String COLUMN_ROOM_NAME = "room_name";
    public static final String COLUMN_ROOM_IMAGE = "room_image";
    public static final String COLUMN_ROOM_MAP = "room_map";

    //Columns of StorageLocation table
    public static final String TABLE_STORAGE_LOCATIONS = "storage_locations";
    public static final String COLUMN_STORAGE_LOCATION_ID = COLUMN_ROOM_ID;
    public static final String COLUMN_STORAGE_LOCATION_NAME = "storage_location_name";
    public static final String COLUMN_STORAGE_LOCATION_ROOM_ID = "room_id";
    public static final String COLUMN_STORAGE_LOCATION_IMAGE = "storage_location_image";
    public static final String COLUMN_STORAGE_LOCATION_MAP = "storage_location_map";

    //Columns of Item table
    public static final String TABLE_ITEMS = "items";
    public static final String COLUMN_ITEM_ID = COLUMN_STORAGE_LOCATION_ID;
    public static final String COLUMN_ITEM_NAME = "item_name";
    public static final String COLUMN_ITEM_DESCRIPTION = "item_description";
    public static final String COLUMN_ITEM_STORAGE_LOCATION_ID = "storage_location_id";
    public static final String COLUMN_ITEM_IMAGE = "item_image";

    private static final String DATABASE_NAME = "rooms.db";
    private static final int DATABASE_VERSION = 8;

    //SQL statement to create Item table
    private static final String SQL_CREATE_TABLE_ITEMS = "CREATE TABLE " + TABLE_ITEMS + "("
            + COLUMN_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_ITEM_NAME + " TEXT NOT NULL, "
            + COLUMN_ITEM_DESCRIPTION + " TEXT NOT NULL, "
            + COLUMN_ITEM_STORAGE_LOCATION_ID + " INTEGER NOT NULL, "
            + COLUMN_ITEM_IMAGE + " BLOB "
            + ");";

    //SQL statement to create StorageLocation table
    private static final String SQL_CREATE_TABLE_STORAGE_LOCATIONS = "CREATE TABLE " + TABLE_STORAGE_LOCATIONS + "("
            + COLUMN_STORAGE_LOCATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_STORAGE_LOCATION_NAME + " TEXT NOT NULL, "
            + COLUMN_STORAGE_LOCATION_ROOM_ID + " INTEGER NOT NULL, "
            + COLUMN_STORAGE_LOCATION_IMAGE + " BLOB, "
            + COLUMN_STORAGE_LOCATION_MAP + " BLOB "
            + ");";

    //SQL statement to create Room table
    private static final String SQL_CREATE_TABLE_ROOMS = "CREATE TABLE " + TABLE_ROOMS + "("
            + COLUMN_ROOM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_ROOM_NAME + " TEXT NOT NULL, "
            + COLUMN_ROOM_IMAGE + " BLOB, "
            + COLUMN_ROOM_MAP + " BLOB "
            + ");";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_ROOMS);
        db.execSQL(SQL_CREATE_TABLE_STORAGE_LOCATIONS);
        db.execSQL(SQL_CREATE_TABLE_ITEMS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("DBHELPER", "Upgrading database from version " + oldVersion + " to "+ newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORAGE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOMS);
        onCreate(db);
    }

}
