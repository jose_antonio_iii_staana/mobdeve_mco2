package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.Function;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.R;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class ListStorageLocationsAdapter extends BaseAdapter {

    private Function function;
    private List<StorageLocation> storageLocations;
    private LayoutInflater inflater;

    public ListStorageLocationsAdapter(Context context, List<StorageLocation> listStorageLocations) {
        this.setItems(listStorageLocations);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().size() : 0 ;
    }

    @Override
    public StorageLocation getItem(int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position) : null ;
    }

    @Override
    public long getItemId(int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position).getStorageLocationId() : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        if (v == null) {
            v = inflater.inflate(R.layout.list_item_storage_location, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textViewStorageLocationName = v.findViewById(R.id.textViewStorageLocationName);
            viewHolder.imageViewStorageLocationimg = v.findViewById(R.id.imageview_storage_location_image_list);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        //Fill row data
        StorageLocation currentItem = getItem(position);
        if(currentItem != null) {
            viewHolder.textViewStorageLocationName.setText(currentItem.getStorageLocationName());
            viewHolder.imageViewStorageLocationimg.setImageBitmap(function.getImage(currentItem.getStorageLocationImage()));
        }
        return v;
    }

    public List<StorageLocation> getItems() {
        return storageLocations;
    }

    public void setItems(List<StorageLocation> storageLocations) {
        this.storageLocations = storageLocations;
    }

    class ViewHolder {
        ImageView imageViewStorageLocationimg;
        TextView textViewStorageLocationName;
    }
}
