package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Item;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class ItemDAO {

    private Context context;

    //Database fields
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allColumns = {
            DBHelper.COLUMN_ITEM_ID,
            DBHelper.COLUMN_ITEM_NAME,
            DBHelper.COLUMN_ITEM_DESCRIPTION,
            DBHelper.COLUMN_ITEM_STORAGE_LOCATION_ID,
            DBHelper.COLUMN_ITEM_IMAGE
    };

    public ItemDAO(Context context) {
        dbHelper = new DBHelper(context);
        this.context = context;

        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Item createItem (String itemName, String itemDescription, long storageLocationId, byte[] image) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_ITEM_NAME, itemName);
        values.put(DBHelper.COLUMN_ITEM_DESCRIPTION, itemDescription);
        values.put(DBHelper.COLUMN_ITEM_STORAGE_LOCATION_ID, storageLocationId);
        values.put(DBHelper.COLUMN_ITEM_IMAGE, image);
        long insertId = database.insert(DBHelper.TABLE_ITEMS, null, values);
        Cursor cursor = database.query(DBHelper.TABLE_ITEMS,
                allColumns,
                DBHelper.COLUMN_ITEM_ID + " = " + insertId,
                null,
                null,
                null,
                null);
        cursor.moveToFirst();
        Item newItem = cursorToItem(cursor);
        cursor.close();
        return newItem;
    }

    public void deleteItem(Item item) {
        long id = item.getItemId();
        System.out.println("Item " + id + " was deleted");
        database.delete(DBHelper.TABLE_ITEMS,
                DBHelper.COLUMN_ITEM_ID + " = " + id,
                null);
    }

    public List<Item> getAllItems() {
        List<Item> listItems = new ArrayList<Item>();

        Cursor cursor = database.query(DBHelper.TABLE_ITEMS,
                allColumns,
                null,
                null,
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Item item = cursorToItem(cursor);
            listItems.add(item);
            cursor.moveToNext();
        }

        cursor.close();
        return listItems;
    }

    public List<Item> getItemsInStorageLocation(long storageLocationId) {
        List<Item> listItems = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.TABLE_ITEMS,
                allColumns, DBHelper.COLUMN_ITEM_STORAGE_LOCATION_ID + " = ?",
                new String[] { String.valueOf(storageLocationId)},
                null,
                null,
                null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Item item = cursorToItem(cursor);
            listItems.add(item);
            cursor.moveToNext();
        }

        cursor.close();
        return listItems;
    }

    private Item cursorToItem(Cursor cursor) {
        Item item = new Item();
        item.setItemId(cursor.getLong(0));
        item.setItemName(cursor.getString(1));
        item.setItemDescription(cursor.getString(2));
        item.setItemimage(cursor.getBlob(4));

        long storageLocationId = cursor.getLong(3);
        StorageLocationDAO storageLocationDAO = new StorageLocationDAO(context);
        StorageLocation storageLocation = storageLocationDAO.getStorageLocationById(storageLocationId);
        if (storageLocation != null)
            item.setStorageLocation(storageLocation);

        return item;
    }
}
