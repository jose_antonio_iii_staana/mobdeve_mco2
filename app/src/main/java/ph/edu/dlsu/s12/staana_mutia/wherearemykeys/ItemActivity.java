package ph.edu.dlsu.s12.staana_mutia.wherearemykeys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters.ListItemsAdapter;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters.ListStorageLocationsAdapter;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.ItemDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.dao.StorageLocationDAO;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Item;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.StorageLocation;

public class ItemActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener {

    public static final String EXTRA_ADDED_ITEM = "extra_key_added_item";
    public static final String EXTRA_SELECTED_STORAGE_LOCATION_ID = "extra_key_selected_storage_storage_location_id";

    private ListView listViewItems;
    private TextView textViewEmptyListItems;
    private Button buttonAddItem;

    private ListItemsAdapter adapter;
    private List<Item> listItems;
    private ItemDAO itemDAO;
    private Context context;

    private long storageLocationId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        //Initialize Views
        this.listViewItems = findViewById(R.id.listview_items);
        this.textViewEmptyListItems = findViewById(R.id.textview_empty_list_items);
        this.buttonAddItem = findViewById(R.id.button_add_item);
        this.listViewItems.setOnItemLongClickListener(this);

        itemDAO = new ItemDAO(this);
        Intent intent = getIntent();
        if (intent != null) {
            this.storageLocationId = intent.getLongExtra(EXTRA_SELECTED_STORAGE_LOCATION_ID, -1);
        }

        if(storageLocationId != -1) {
            listItems = itemDAO.getItemsInStorageLocation(storageLocationId);

            //Fill ListView
            context = this;
            if (listItems != null && !listItems.isEmpty()) {
                adapter = new ListItemsAdapter(this, listItems);
                listViewItems.setAdapter(adapter);
            } else {
                //Placeholder if there are no Storage Locations
                textViewEmptyListItems.setVisibility(View.VISIBLE);
                listViewItems.setVisibility(View.GONE);
            }

            ActivityResultLauncher<Intent> addItemActivityResultLauncher = registerForActivityResult(
                    new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            if(data != null) {
                                Item newItem = (Item) data.getSerializableExtra(EXTRA_ADDED_ITEM);

                                //Add Item to List
                                if(newItem != null) {
                                    if(listItems == null) {
                                        listItems = new ArrayList<>();
                                    }
                                    listItems.add(newItem);

                                    //Toggle placeholder
                                    if(listViewItems.getVisibility() != View.VISIBLE) {
                                        listViewItems.setVisibility(View.VISIBLE);
                                        textViewEmptyListItems.setVisibility(View.GONE);
                                    }

                                    if(adapter == null) {
                                        adapter = new ListItemsAdapter(context, listItems);
                                        listViewItems.setAdapter(adapter);
                                    }

                                    //Refresh ListView
                                    adapter.setItems(listItems);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    });

            buttonAddItem.setOnClickListener(v -> {
                Intent addStorageLocationIntent = new Intent(ItemActivity.this, AddItemActivity.class);
                addStorageLocationIntent.putExtra("STORAGE_LOCATION_ID", storageLocationId);
                addItemActivityResultLauncher.launch(addStorageLocationIntent);
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        itemDAO.close();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Item clickedItem = adapter.getItem(position);
        showDeleteDialog(clickedItem);
        return true;
    }

    private void showDeleteDialog(final Item clickedItem) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Delete");
        alertDialogBuilder.setMessage("Delete \"" + clickedItem.getItemName() + "\" from your list of items?");

        alertDialogBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            //Delete clickedStorageLocation and Refresh ListView
            if(itemDAO != null) {
                itemDAO.deleteItem(clickedItem);
                listItems.remove(clickedItem);

                //Refresh ListView
                if(listItems.isEmpty()) {
                    listItems = null;
                    listViewItems.setVisibility(View.GONE);
                    textViewEmptyListItems.setVisibility(View.VISIBLE);
                }
                adapter.setItems(listItems);
                adapter.notifyDataSetChanged();
            }

            dialog.dismiss();
            Toast.makeText(ItemActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();
        });

        alertDialogBuilder.setNeutralButton(android.R.string.no, (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
