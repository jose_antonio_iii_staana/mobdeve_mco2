package ph.edu.dlsu.s12.staana_mutia.wherearemykeys.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.List;

import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.Function;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.R;
import ph.edu.dlsu.s12.staana_mutia.wherearemykeys.models.Room;

public class ListRoomsAdapter extends BaseAdapter {

    private Function function;
    private List<Room> items;
    private LayoutInflater layoutInflater;

    public ListRoomsAdapter(Context context, List<Room> listRooms) {
        this.setItems(listRooms);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().size() : 0 ;
    }

    @Override
    public Room getItem(int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position) : null ;
    }

    @Override
    public long getItemId (int position) {
        return (getItems() != null && !getItems().isEmpty()) ? getItems().get(position).getRoomId() : position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(R.layout.list_item_room, parent, false);
            holder = new ViewHolder();
            holder.textViewRoomName = v.findViewById(R.id.textViewRoomName);
            holder.imageViewRoomimg = v.findViewById(R.id.imageViewRoom); //Image
            //Map
            v.setTag(holder);
        }
        else {
            holder = (ViewHolder) v.getTag();
        }

        //Fill rows with data
        Room currentRoom = getItem(position);
        if(currentRoom != null) {
            holder.textViewRoomName.setText(currentRoom.getRoomName());
            holder.imageViewRoomimg.setImageBitmap(function.getImage(currentRoom.getRoomImage())); //Image
            //Map
        }

        return v;
    }

    public List<Room> getItems() {
        return items;
    }

    public void setItems(List<Room> items) {
        this.items = items;
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }


    class ViewHolder {
        ImageView imageViewRoomimg;
        TextView textViewRoomName;
    }
}
